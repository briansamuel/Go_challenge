package main

import (
	"fmt"
	"sync"
	"time"
)


func startWorker(queue chan int, name string, wg *sync.WaitGroup) {
	defer wg.Done()
	for i := range queue {
	
		fmt.Printf("Worker %s is crawling URL %d \n", name, i)
		time.Sleep(time.Second/10)
	}

	fmt.Printf("Worker %s has finish \n", name)
}

func main() {
	fmt.Println("Crawler Challenge")

	numURL := 100
	numOfWorker := 2
	wg := new(sync.WaitGroup)
	wg.Add(numOfWorker)
	// Make Buffered Chan
	queue := make(chan int, numURL)

	

	go func()  {
		for i := 1; i <= numURL; i++ {
			
			queue <- i
			fmt.Printf("Add URL %d to Queue \n", i)
		}
		close(queue)
	}()

	for i := 1; i <= numOfWorker; i++ {
		go func(s string){
			startWorker(queue, s, wg)
		}(fmt.Sprintf("%d", i))
	}

	wg.Wait()

	fmt.Println("All worker done")
	

}